#include <QGuiApplication>
#include <QtGlobal>
#include <QQmlEngine>
#include <QQuickView>
#include <QResource>

const char*const applicationName="qtissue";

int main(int argc,char* argv[]) {

  QGuiApplication app(argc,argv);  
  app.setApplicationName(applicationName);
  app.setOrganizationName("Qt fanclub");
  app.setOrganizationDomain("com.example");

  if (!QResource::registerResource("MyAppContent.rcc")) {
    qFatal("Couldn't register MyAppContent.rcc resource file");
  }

  QQuickView viewer;

  // Workround for https://forum.qt.io/topic/72394/including-qml-js-non-resource-files-from-qml-in-a-resource-expected-behaviour
  //viewer.engine()->addImportPath("qrc:/");      // Works on Qt 5.7.  Works on Qt 5.9.4.
  viewer.engine()->addImportPath("qrc://");     // Works on Qt 5.7.  Fails on Qt 5.9.4.
  //viewer.engine()->addImportPath("qrc:///");    // Works on Qt 5.7.  Works on Qt 5.9.4.

  viewer.setResizeMode(QQuickView::SizeRootObjectToView);
  viewer.resize(QSize(640,480));
  viewer.setSource(QUrl("qrc:///main.qml"));
  viewer.show();

  return app.exec();
}
