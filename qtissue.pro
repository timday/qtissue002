TEMPLATE = app

CONFIG += release
CONFIG += qtquickcompile

QT += core gui qml quick svg

TARGET = qtissue

SOURCES += main.cpp
RESOURCES += qtissue.qrc

mac {
  CONTENT_DATA.files += MyAppContent.rcc
  QMAKE_BUNDLE_DATA += CONTENT_DATA
}

cache()
