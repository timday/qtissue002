Minimal demo of a Qt issue
==========================

For investigation of an issue where QML module imports appear to suddenly have their case squashed between Qt 5.7(.0) and 5.7.1 (`import MyAppContent 1.0` started producing an error message about `myappcontent`).

The module is in `MyAppContent` and is compiled to `MyAppContent.rcc` in the build scripts; this is then explicitly `QResource::registerResource`-ed in the `main.cpp`.  This is mainly just to mirror the behaviour of the app this issue was originally detected in... I'd be surprised if compiled-in resources were any more immune to the issue though.

Status
------

Think this is now understood.  See <https://forum.qt.io/topic/88004/something-about-module-import-paths-changed-in-qt5-9-1-uppercase-squashed-to-lowercase>.

Build
-----

NB First you will need to change, in whichever build script you use:

* The path to your Qt dir (and possibly version) in those scripts.  

Build with:

* **OSX**: `./MAKE-mac` (builds `qtissue.app`)
* **Linux**: `./MAKE-linux` builds `./qtissue` (reference Debian 8.10 (Jessie); NB *not* expected to work with Debian's Qt4!)

NB Use `make distclean` to clean up *before* changing the Qt version used.
