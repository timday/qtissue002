import QtQuick 2.5

import MyAppContent 1.0

Rectangle {
  id: main
  width: 640
  height: 480
  color: 'blue'

  Text {
    anchors.centerIn: parent
    color: 'yellow'
    text: MyMessages.message()
  }
}
